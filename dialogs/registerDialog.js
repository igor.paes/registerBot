const { WaterfallDialog, ComponentDialog, TextPrompt, NumberPrompt, DialogTurnStatus, DialogSet } = require('botbuilder-dialogs');
const cpf = require('@fnando/cpf');
const validateDate = require('validate-date');
const axios = require('axios').default;
const dedent = require('dedent');

const TEXT_PROMPT = 'TEXT_PROMPT';
const NUMBER_PROMPT = 'NUMBER_PROMPT';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';
const CPF = 'CPF_PROMPT';
const DATA = 'DATE_PROMPT';
const CEP = 'CEP_PROMPT';

const endDialog = '';

class RegisterDialog extends ComponentDialog {
    constructor(conversationState, userState, luisRecognizer) {
        super('registerDialog');

        this.addDialog(new TextPrompt(TEXT_PROMPT));
        this.addDialog(new NumberPrompt(NUMBER_PROMPT));
        this.addDialog(new TextPrompt(DATA, this.handleDate.bind(this)));
        this.addDialog(new TextPrompt(CPF, this.handleCPF.bind(this)));
        this.addDialog(new TextPrompt(CEP, this.handleCEP.bind(this)));
        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.getName.bind(this),
            this.getAge.bind(this),
            this.getGender.bind(this),
            this.getCPF.bind(this),
            this.getCEP.bind(this),
            this.getBirthday.bind(this),
            this.summary.bind(this)

        ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }

    /**
     *
     * @param {TurnContext} context
     * @param {StatePropertyAccessor<DialogState>} acessor
     * @param {object} entities
     */
    async run(context, acessor, entities) {
        const dialogSet = new DialogSet(acessor);
        dialogSet.add(this);
        const dialogContext = await dialogSet.createContext(context);
        const result = await dialogContext.continueDialog();
        if (result.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id, entities);
        }
    }

    /**
     *
     * @param { stepContext } context
     * @returns {Promise<*>}
     */
    async getName(stepContext) {
        if (stepContext._info.options.$instance.number) {
            const number = stepContext._info.options.$instance.number;
            const val = parseInt(number[0].text);
            console.log(number);
            if (val === undefined || val === 0 || val === ' ') {
                return stepContext.prompt(TEXT_PROMPT, 'Digite seu nome');
            } else if (val < 150) {
                console.log(number[0].text);
                stepContext.values.age = val;
                return stepContext.prompt(TEXT_PROMPT, 'Digite seu nome');
            }
        }
        return stepContext.prompt(TEXT_PROMPT, 'Digite seu nome');
    }

    /**
     *
     * @param { stepContext } context
     * @returns {Promise<*>}
     */
    async getAge(stepContext) {
        stepContext.values.nome = stepContext.result;
        if (stepContext.values.age === undefined) {
            return stepContext.prompt(NUMBER_PROMPT, 'Digite sua idade');
        } else {
            return stepContext.next();
        }
    }

    /**
     *
     * @param { stepContext } context
     * @returns {Promise<*>}
     */
    async getGender(stepContext) {
        stepContext.values.age = stepContext.result;
        return stepContext.prompt(TEXT_PROMPT, 'Digite seu genero (masculino, feminino, outro)');
    }

    /**
     *
     * @param { stepContext } context
     * @returns {Promise<*>}
     */
    async getCPF(stepContext) {
        stepContext.values.gender = stepContext.result;
        return stepContext.prompt(CPF, 'Digite seu CPF');
    }

    /**
     *
     * @param { stepContext } context
     * @returns {Promise<*>}
     */
    async getCEP(stepContext) {
        stepContext.values.cpf = stepContext.result;
        return stepContext.prompt(CEP, 'Digite seu CEP');
    }

    /**
     *
     * @param { stepContext } context
     * @returns {Promise<*>}
     */
    async getBirthday(stepContext) {
        stepContext.values.cep = stepContext.result;
        return stepContext.prompt(DATA, 'Digite sua data de aniversário Ex:(11/11/1990)');
    }

    /**
     *
     * @param { stepContext } context
     * @returns {Promise<*>}
     */
    async summary(stepContext) {
        stepContext.values.birthday = stepContext.result;

        const cep = this.searchCep(stepContext.values.cep);

        try {
            const cidade = await cep.then((response) => {
                return response.data;
            }).catch((error) => {
                console.log(error);
            });

            await stepContext.context.sendActivity(
                `Seu nome é ${ stepContext.values.nome }, você nasceu no dia ${ stepContext.values.birthday } seu gênero é ${ stepContext.values.gender }.
                            Seu CPF é ${ stepContext.values.cpf }, e você reside no endereço ${ cidade.localidade } - ${ cidade.uf }.
                            \n Seja bem vindo! 😊`);
        } catch (error) {
            console.log(error);
        }

        return stepContext.endDialog();
    }

    /**
     *
     * @param promptContext
     * @returns {Promise<boolean>}
     */
    async handleCPF(promptContext) {
        console.log('Testando' + promptContext.recognized.value);
        const validate = cpf.isValid(promptContext.recognized.value);
        if (validate === true) {
            return promptContext.recognized.succeeded;
        } else {
            promptContext.context.sendActivity('CPF INVÁLIDO');
            return false;
        }
    }

    /**
     *
     * @param promptContext
     * @returns {Promise<boolean>}
     */
    async handleDate(promptContext) {
        var validate = validateDate(promptContext.recognized.value);
        if (validate === 'Valid Date') {
            return promptContext.recognized.succeeded;
        } else {
            promptContext.context.sendActivity('Data Inválida. Ex: (11/11/1997)');
            return false;
        }
    }

    /**
     *
     * @param promptContext
     * @returns {Promise<boolean>}
     */
    async handleCEP(promptContext) {
        const cep = this.searchCep(promptContext.recognized.value);
        try {
            var cidade = await cep.then((response) => {
                return response;
            }).catch((error) => {
                console.log(error);
            });
            console.log('cidade' + cidade);
            if (cidade === undefined) {
                promptContext.context.sendActivity('CEP inválido');
                return false;
            } else {
                return promptContext.recognized.succeeded;
            }
        } catch (error) {
            console.log(error);
        }
    }

    /**
     *
     * @returns { string }
     */
    async isDialogComplete() {
        return endDialog;
    }

    /**
     *
     * @param { cep } String
     * @returns {Promise<AxiosResponse<any>>}
     */
    async searchCep(cep) {
        return axios.get('http://viacep.com.br/ws/' + cep + '/json/');
    }
}

module.exports.RegisterDialog = RegisterDialog;
