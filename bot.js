// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ActivityHandler, MessageFactory, ConversationState, UserState, MemoryStorage } = require('botbuilder');
const {RegisterDialog} = require('./dialogs/registerDialog')
const {LUISRecognizer, LuisRecognizer} = require('botbuilder-ai')



class RegisterBot extends ActivityHandler {

    constructor(conversationState,userState,dialog) {
        super();
        /**
     *
     * @param {ConversationState} conversationState
     * @param {UserState} userState
     * @param {Dialog} dialog
     */
        
        this.conversationState = conversationState;
        this.conversationState = userState;
        this.dialog = dialog;
        this.dialogState = this.conversationState.createProperty('DialogState')
        this.registerDialog = new RegisterDialog(this.conversationState,this.userState);
        // See https://aka.ms/about-bot-activity-message to learn more about the message and other activity types.
        this.previousIntent = this.conversationState.createProperty("previousIntent");
        this.conversationData = this.conversationState.createProperty("conversationData")


        const dispatchRecognizer = new LuisRecognizer({
            applicationId: process.env.LuisAppId,
            endpointKey: process.env.LuisAPIKey,
            endpoint: `https://${process.env.LuisAPIHostName}.api.cognitive.microsoft.com`
        },{
            includeAllIntents: true,
            verbose: true,
            includeInstanceData: true
        }, true)

    

       

        this.onMessage(async (context, next) => {
            const luisResult = await dispatchRecognizer.recognize(context)
            const intent = LuisRecognizer.topIntent(luisResult)
            const entities = luisResult.entities;

            await this.dispatchToIntentAsync(context,intent, entities)
            await next();
        });

        this.onMembersAdded(async (context, next) => {
            const membersAdded = context.activity.membersAdded;
            const welcomeText = 'Hello and welcome!';
            for (let cnt = 0; cnt < membersAdded.length; ++cnt) {
                if (membersAdded[cnt].id !== context.activity.recipient.id) {
                    await context.sendActivity(MessageFactory.text(welcomeText, welcomeText));
                }
            }
            // By calling next() you ensure that the next BotHandler is run.
            await next();
        });

        this.onDialog(async (context, next) => {
            // Save any state changes. The load happened during the execution of the Dialog.
            await conversationState.saveChanges(context, false);
            await userState.saveChanges(context, false);
      
            // By calling next() you ensure that the next BotHandler is run.
            await next();
          });

    }
    

    async dispatchToIntentAsync(context,intent, entities){

        var currentIntent = '';
        const previousIntent = await this.previousIntent.get(context,{})
        const conversationData = await this.conversationData.get(context,{})

        if(previousIntent.intentName && conversationData.endDialog === false){
            currentIntent = previousIntent.intentName
        }else if(previousIntent.intentName && conversationData.endDialog === true){
            currentIntent = intent;
        }else{
            currentIntent = intent;
            await this.previousIntent.set(context, {intentName: intent})
        }
         await this.conversationData.set(context,{endDialog: false})
         await this.registerDialog.run(context,this.dialogState, entities)
         conversationData.endDialog = await this.registerDialog.isDialogComplete();
        if(conversationData.endDialog){
            await this.previousIntent.set(context, {intentName: null})
        }
    }
}

module.exports.RegisterBot = RegisterBot;
